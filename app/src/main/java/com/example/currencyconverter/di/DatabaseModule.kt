package com.example.currencyconverter.di

import android.content.Context
import com.example.currencyconverter.framework.room_db.Dao
import com.example.currencyconverter.framework.room_db.Database
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Provides
    fun provideDao(appDatabase: Database): Dao {
        return appDatabase.dao()
    }

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext appContext: Context): Database {
        return Database.getDatabase(appContext)
    }
}