package com.example.currencyconverter.di

import com.example.currencyconverter.data.repositories.ConverterRepository
import com.example.currencyconverter.interactors.ConvertUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object InterectorModule {

    @Singleton
    @Provides
    fun provideConverter(
        converterRepository: ConverterRepository
    ): ConvertUseCase{
        return ConvertUseCase(converterRepository)
    }

}