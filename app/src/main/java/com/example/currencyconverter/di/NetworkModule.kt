package com.example.currencyconverter.di

import com.example.currencyconverter.data.repositories.ConverterRepository
import com.example.currencyconverter.framework.datasources.ConverterDataSourceImpl
import com.example.currencyconverter.framework.repositories.ConverterRepositoryImpl
import com.example.currencyconverter.framework.retrofit.ConvertService


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun createGsonBuilder(): Gson {
        return GsonBuilder()
            .create()
    }

    @Singleton
    @Provides
    fun getRetrofitInstance(gson:  Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl("http://apilayer.net/")
            .addConverterFactory(GsonConverterFactory.create(gson))

    }


    @Singleton
    @Provides
    fun getConvertApiService (builder : Retrofit.Builder) : ConvertService{
        return builder
            .build()
            .create(ConvertService::class.java)
    }

    @Singleton
    @Provides
    fun getConverterRepository(converterDataSourceImpl: ConverterDataSourceImpl) : ConverterRepository{
        return ConverterRepositoryImpl(converterDataSourceImpl)
    }



}