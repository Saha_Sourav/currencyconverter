package com.example.currencyconverter.interactors

import com.example.currencyconverter.data.repositories.ConverterRepository
import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.Converter
import com.example.currencyconverter.util.Resource

class ConvertUseCase (private val converterRepository: ConverterRepository) {

    suspend operator fun invoke(convertRequest: ConvertRequest): Resource<Converter> =
        converterRepository.makeConvertRequest(convertRequest)
}
