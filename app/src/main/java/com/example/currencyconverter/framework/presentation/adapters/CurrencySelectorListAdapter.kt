package com.example.currencyconverter.framework.presentation.adapters
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.databinding.ListItemCurrencyBinding
import com.example.currencyconverter.framework.presentation.activities.ConverterActivity
import com.example.currencyconverter.framework.presentation.utils.Constants
import com.example.currencyconverter.framework.presentation.viewholders.CurrencySelectorViewHolder
import com.example.currencyconverter.util.Utils
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class CurrencySelectorListAdapter(private val currencyNameList : ArrayList<String>) : RecyclerView.Adapter<CurrencySelectorViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencySelectorViewHolder {
        val binding = ListItemCurrencyBinding.inflate(LayoutInflater.from(parent.context))
        return CurrencySelectorViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrencySelectorViewHolder, position: Int) {
        holder.bind(currencyNameList[position])
        holder.itemView.setOnClickListener {
            ( Utils.unwrap(holder.itemView.context) as ConverterActivity).supportFragmentManager.setFragmentResult(Constants.FRAGMENT_RESULT,
                Bundle().apply {
                    putString(Constants.CURRENCY, currencyNameList[position])
                })
        }
    }

    override fun getItemCount(): Int {
        return currencyNameList.size
    }

}