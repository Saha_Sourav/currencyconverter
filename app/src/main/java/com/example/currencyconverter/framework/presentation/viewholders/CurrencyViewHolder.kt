package com.example.currencyconverter.framework.presentation.viewholders
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.R
import com.example.currencyconverter.databinding.ListItemCurrencyBinding
import com.example.currencyconverter.domain.model.CurrencyWithValue

class CurrencyViewHolder(private val binding : ListItemCurrencyBinding) : RecyclerView.ViewHolder(binding.root) {


    fun bind(currencyWithValue: CurrencyWithValue){
        binding.currencyTextView.text = binding.root.context.getString(R.string.currency_with_value,currencyWithValue.currencyName,currencyWithValue.currecncyValue)
    }
}