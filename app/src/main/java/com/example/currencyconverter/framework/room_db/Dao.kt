package com.example.currencyconverter.framework.room_db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.currencyconverter.domain.model.ConverterLocal


@Dao
abstract class Dao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertRemoteConversionRateDataToDb(converterLocal: ConverterLocal):Long

    @Query("SELECT * FROM converter_local WHERE source = :source")
    abstract suspend fun getConversionRateBySource(source:String): List<ConverterLocal?>?

    @Query("update converter_local set time_stamp = :time ,quote_json = :quote where source = :source")
    abstract suspend fun updateConverterLocalData(time:Long,quote : String,source:String):Int

}