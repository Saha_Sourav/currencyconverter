package com.example.currencyconverter.framework.presentation.viewmodels


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.Converter
import com.example.currencyconverter.domain.model.CurrencyWithValue
import com.example.currencyconverter.interactors.ConvertUseCase
import com.example.currencyconverter.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class ConverterViewModel @Inject constructor(private val convertUseCase: ConvertUseCase) :
    ViewModel() {

    var converterLiveData : MutableLiveData<Resource<Converter>> = MutableLiveData()
    var currencyMap : HashMap<String,Float>? = HashMap()
    var currentCurrency : String? = null

    fun makeConvertRequest(convertRequest: ConvertRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            currentCurrency = convertRequest.source
            val response = convertUseCase.invoke(convertRequest)
            response.data?.quotes?.let {nonNullMap ->
                currencyMap = nonNullMap
            }
            converterLiveData.postValue(response)
        }
    }

    fun getCurrencyListWithValueFromMap(map : HashMap<String,Float>,multiplier: Float) : ArrayList<CurrencyWithValue>{
        val keySet = map.keys
        val arrayList : ArrayList<CurrencyWithValue> = ArrayList()
        for(key in keySet){
            val kj = map[key]
            kj?.let {nonNullValue ->
                arrayList.add(CurrencyWithValue(getTargetCurrencyName(key),
                    (nonNullValue*multiplier)
                ))
            }
        }
        return arrayList
    }

    fun validateInput(text : String?):Boolean{
        return if(text == null) false
        else{
            if(text.isEmpty()) false
            else{
                !text.endsWith(".")
            }
        }
    }

    private fun getTargetCurrencyName(mergedCurrencyName : String) : String{
        return mergedCurrencyName.substring(3)
    }

    fun getCurrencyNameListFromSource(map : HashMap<String,Float>) : ArrayList<String>{
        val keySet = map.keys
        val arrayList : ArrayList<String> = ArrayList()
        for(key in keySet){
            map[key]?.let { _ ->
                arrayList.add(getTargetCurrencyName(key))
            }
        }
        return arrayList
    }
}