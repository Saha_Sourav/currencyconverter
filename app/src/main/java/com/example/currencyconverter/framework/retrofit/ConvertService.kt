package com.example.currencyconverter.framework.retrofit

import com.example.currencyconverter.domain.model.Converter
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ConvertService {

    @GET("api/live")
    suspend fun makeConvertRequest(
        @Query("access_key") accessKey : String,
        @Query("source") source : String,
        @Query("format") format : Long
    ): Response<Converter>
}