package com.example.currencyconverter.framework.room_db

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.currencyconverter.domain.model.ConverterLocal

@androidx.room.Database(entities = [
    ConverterLocal::class
], version = 1, exportSchema = true)

abstract class Database : RoomDatabase() {

    abstract fun dao(): Dao

    companion object {
        private var instance: Database? = null
        private const val NAME = "main_db"

        fun getDatabase(context: Context): Database {
            synchronized(this) {
                if (instance == null) {
                    instance =
                        Room.databaseBuilder(context.applicationContext, Database::class.java, NAME)
                            .build()
                }
            }
            return instance!!
        }
    }
}