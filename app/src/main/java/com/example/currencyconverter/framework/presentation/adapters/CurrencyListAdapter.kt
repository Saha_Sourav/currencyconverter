package com.example.currencyconverter.framework.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.databinding.ListItemCurrencyBinding
import com.example.currencyconverter.domain.model.CurrencyWithValue
import com.example.currencyconverter.framework.presentation.viewholders.CurrencyViewHolder

class CurrencyListAdapter(private val currencyList : ArrayList<CurrencyWithValue>) : RecyclerView.Adapter<CurrencyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = ListItemCurrencyBinding.inflate(LayoutInflater.from(parent.context))
        return CurrencyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencyList[position])
    }

    override fun getItemCount(): Int {
       return currencyList.size
    }
}