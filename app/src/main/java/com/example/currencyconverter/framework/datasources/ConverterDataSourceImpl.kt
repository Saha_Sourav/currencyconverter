package com.example.currencyconverter.framework.datasources

import com.example.currencyconverter.data.datasources.ConverterDataSource
import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.Converter
import com.example.currencyconverter.domain.model.ConverterLocal
import com.example.currencyconverter.framework.presentation.utils.Constants
import com.example.currencyconverter.framework.retrofit.ConvertService
import com.example.currencyconverter.framework.room_db.Dao
import com.example.currencyconverter.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import com.google.gson.Gson




class ConverterDataSourceImpl @Inject constructor(private val convertService: ConvertService,val dao:Dao) : ConverterDataSource {
    override suspend fun makeConvertRequest(convertRequest: ConvertRequest): Resource<Converter> =
        withContext(Dispatchers.IO) {
            val converterLocal  = checkIfNeedToCallApi(convertRequest.source)

            if(converterLocal==null){
                try {
                    ///todo change hard coded token///
                    val response = convertService.makeConvertRequest("c30938b847681f9202f0af59f1506439",convertRequest.source,convertRequest.format)
                    if (response.isSuccessful) {
                        val success = response.body()?.success
                        if(success!!) {
                            insertOrUpdateLocalData(response.body()!!)
                            return@withContext Resource.success(response.body())
                        }else{
                            val error = response.body()?.error
                            return@withContext Resource.error(
                                error?.info,
                                null
                            )
                        }
                    } else {
                        when {
                            response.code() == Constants.HTTP_STATUS_CODE_BAD_REQUEST -> {
                                return@withContext Resource.error(Constants.HTTP_BAD_REQUEST_MESSAGE, null)
                            }
                            response.code() == Constants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR -> {
                                return@withContext Resource.error(
                                    Constants.HTTP_INTERNAL_SERVER_ERROR_MESSAGE,
                                    null
                                )
                            }
                            else -> {
                                return@withContext Resource.error(response.message(), null)
                            }
                        }
                    }
                } catch (e: Exception) {
                    if (e is ConnectException || e is TimeoutException || e is SocketTimeoutException || e is UnknownHostException) {
                        return@withContext Resource.exception(Constants.NO_NETWORK_MESSAGE, null)
                    } else {
                        return@withContext Resource.exception(Constants.SOMETHING_WENT_WRONG, null)

                    }
                }
            }else{
                try{
                    return@withContext Resource.success(convertLocalToRemoteData(converterLocal))

                }catch (e : Exception){
                    return@withContext Resource.exception(Constants.SOMETHING_WENT_WRONG, null)
                }
            }

        }


    private suspend fun checkIfNeedToCallApi(source : String):ConverterLocal?{
        val conversionRecordList = dao.getConversionRateBySource(source)
        return if(conversionRecordList == null || conversionRecordList.isEmpty()) null
        else{
            val diff = System.currentTimeMillis() - conversionRecordList[0]?.timeStamp!!
            if(diff>30*60*1000){
                null
            }else{
                conversionRecordList[0]
            }
        }
    }

    private suspend fun insertOrUpdateLocalData(conversionData : Converter){
        val conversionRateList = dao.getConversionRateBySource(conversionData.source!!)
        if(conversionRateList == null || conversionRateList.isEmpty() ) {
            val quoteJson = JSONObject(conversionData.quotes?.toMap())
            //using non-null operator because its not possible to get a null value here if api call is success
            val convertLocal = ConverterLocal(
                conversionData.source,
                System.currentTimeMillis(),
                quoteJson.toString()
            )
            dao.insertRemoteConversionRateDataToDb(convertLocal)

        }else{
            val conversionRateLocalData = conversionRateList[0]
            dao.updateConverterLocalData(System.currentTimeMillis(),JSONObject(conversionData.quotes?.toMap()).toString(),conversionRateLocalData?.source!!)
        }

    }

    private  fun convertLocalToRemoteData(converterLocal: ConverterLocal):Converter{
        val quoteJson = converterLocal.quoteJson
        val hashMap : HashMap<String, Any>  =  Gson().fromJson(quoteJson, HashMap::class.java) as HashMap<String, Any>

        val keys = hashMap.keys
        for(key in keys){
            val value = hashMap[key]
            hashMap[key] = value.toString().toFloat()
        }
        return Converter(true,null,converterLocal.timeStamp,converterLocal.source,hashMap as HashMap<String, Float>)
    }


}