package com.example.currencyconverter.framework.repositories

import com.example.currencyconverter.data.datasources.ConverterDataSource
import com.example.currencyconverter.data.repositories.ConverterRepository
import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.Converter
import com.example.currencyconverter.util.Resource

class ConverterRepositoryImpl(private val converterDataSource: ConverterDataSource) : ConverterRepository {
    override suspend fun makeConvertRequest(convertRequest: ConvertRequest): Resource<Converter> {
        return converterDataSource.makeConvertRequest(convertRequest)
    }
}