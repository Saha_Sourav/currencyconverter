package com.example.currencyconverter.framework.presentation.viewholders

import androidx.recyclerview.widget.RecyclerView
import com.example.currencyconverter.databinding.ListItemCurrencyBinding

class CurrencySelectorViewHolder(private val binding : ListItemCurrencyBinding) : RecyclerView.ViewHolder(binding.root) {


    fun bind(currencyName : String){
        binding.currencyTextView.text = currencyName
    }
}