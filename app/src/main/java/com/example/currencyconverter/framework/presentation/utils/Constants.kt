package com.example.currencyconverter.framework.presentation.utils

object Constants {
    const val CURRENCY_LIST = "CURRENCY_LIST"
    const val CURRENCY = "CURRENCY"
    const val SOMETHING_WENT_WRONG = "Something went wrong"
    const val FRAGMENT_RESULT = "FRAGMENT_RESULT"


    //
    const val HTTP_STATUS_CODE_BAD_REQUEST = 400
    const val HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR = 500

    const val HTTP_BAD_REQUEST_MESSAGE = "Bad Request"
    const val HTTP_INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error"

    const val NO_NETWORK_MESSAGE = "No network connection ,  Please Check your connection"


}