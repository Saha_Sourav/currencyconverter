package com.example.currencyconverter.framework.presentation.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.currencyconverter.R
import com.example.currencyconverter.databinding.FragmentConverterBinding
import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.CurrencyWithValue
import com.example.currencyconverter.framework.presentation.adapters.CurrencyListAdapter
import com.example.currencyconverter.framework.presentation.utils.Constants
import com.example.currencyconverter.framework.presentation.viewmodels.ConverterViewModel
import com.example.currencyconverter.util.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ConverterListFragment : Fragment(R.layout.fragment_converter) {

    private val converterViewModel :ConverterViewModel by viewModels()
    private var currencyAdapter : CurrencyListAdapter? = null
    private lateinit var binding : FragmentConverterBinding
    private var currencySelectorBottomSheet : CurrencySelectorBottomSheet? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentConverterBinding.bind(view)
        observeInitialData()
        getCurrencyList("USD")
        binding.currencyNameTextView.setOnClickListener {
            if(converterViewModel.validateInput(binding.currencyInputEditText.text?.toString())){
                showCurrencyNameBottomSheet()
            }else{
                Toast.makeText(requireContext(),getString(R.string.enter_a_valid_value),Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getCurrencyList(source : String){
        binding.loaderLayout.visibility = View.VISIBLE
        converterViewModel.makeConvertRequest(ConvertRequest(source,1))
    }

    private fun showCurrencyNameBottomSheet(){
        currencySelectorBottomSheet = CurrencySelectorBottomSheet()
        currencySelectorBottomSheet?.arguments = Bundle().apply {
            converterViewModel.currencyMap?.let {nonNullCurrencyMap ->
                putStringArrayList(Constants.CURRENCY_LIST,converterViewModel.getCurrencyNameListFromSource(nonNullCurrencyMap))
            }
        }
        activity?.let { nonNullActivity ->
            nonNullActivity.supportFragmentManager.setFragmentResultListener(Constants.FRAGMENT_RESULT,viewLifecycleOwner,{_ , bundle ->
                val currencyName = bundle.getString(Constants.CURRENCY)
                currencyName?.let {
                    if(converterViewModel.validateInput(binding.currencyInputEditText.text?.toString())) {
                        currencySelectorBottomSheet?.dismiss()
                        getCurrencyList(it)
                    }
                }
            })
        }
        activity?.supportFragmentManager?.let {nonNullFragmentManager ->
            currencySelectorBottomSheet?.show(nonNullFragmentManager,"show")

        }
    }

    private fun observeInitialData(){
        converterViewModel.converterLiveData.observe(viewLifecycleOwner,  {
            binding.loaderLayout.visibility = View.GONE
            when(it.status){
                Status.SUCCESS ->{
                    val currencyResponse = it.data
                    binding.currencyNameTextView.text = converterViewModel.currentCurrency
                    currencyResponse?.let {nonNullCurrencyResponse ->
                        nonNullCurrencyResponse.quotes?.let { nonNullMap ->
                            initAdapter(converterViewModel.getCurrencyListWithValueFromMap(nonNullMap,binding.currencyInputEditText.text.toString().toFloat()))
                        }
                    }
                }

                Status.ERROR ->{
                    context?.let { nonNullContext ->
                        Toast.makeText(nonNullContext,it.message,Toast.LENGTH_LONG).show()
                    }
                }
                Status.EXCEPTION ->{
                    context?.let { nonNullContext ->
                        Toast.makeText(nonNullContext,it.message,Toast.LENGTH_LONG).show()
                    }
                }

            }
        })
    }

    private fun initAdapter(currencyList : ArrayList<CurrencyWithValue>){
        currencyAdapter = CurrencyListAdapter(currencyList)
        binding.currencyListRecyclerView.adapter = currencyAdapter
        binding.currencyListRecyclerView.layoutManager = LinearLayoutManager(context)
    }
}