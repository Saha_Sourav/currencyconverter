package com.example.currencyconverter.framework.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.currencyconverter.databinding.FragmentCurrencySelectorBottomSheetBinding
import com.example.currencyconverter.framework.presentation.adapters.CurrencySelectorListAdapter
import com.example.currencyconverter.framework.presentation.utils.Constants
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class CurrencySelectorBottomSheet : BottomSheetDialogFragment() {

    private lateinit var binding : FragmentCurrencySelectorBottomSheetBinding
    private var currencyNameList : ArrayList<String>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCurrencySelectorBottomSheetBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initArguments()
        initAdapter()
    }

    private fun initArguments(){
        currencyNameList = arguments?.getStringArrayList(Constants.CURRENCY_LIST)
    }

    private fun initAdapter(){
        currencyNameList?.let { nonNullCurrencyList ->
            val adapter = CurrencySelectorListAdapter(nonNullCurrencyList)
            binding.currencySelectorList.adapter =adapter
            binding.currencySelectorList.layoutManager = LinearLayoutManager(context)

        }
    }
}