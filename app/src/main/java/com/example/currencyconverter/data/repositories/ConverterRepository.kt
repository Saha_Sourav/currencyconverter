package com.example.currencyconverter.data.repositories

import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.Converter
import com.example.currencyconverter.util.Resource

interface ConverterRepository {

    suspend fun makeConvertRequest(convertRequest: ConvertRequest) : Resource<Converter>

}