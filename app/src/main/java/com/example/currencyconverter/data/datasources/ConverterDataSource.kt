package com.example.currencyconverter.data.datasources

import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.Converter
import com.example.currencyconverter.util.Resource

interface ConverterDataSource {

    suspend fun makeConvertRequest(convertRequest: ConvertRequest): Resource<Converter>

}