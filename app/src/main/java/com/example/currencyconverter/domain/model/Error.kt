package com.example.currencyconverter.domain.model

data class Error(
    val code : Int,
    val info: String
)