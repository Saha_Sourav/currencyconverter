package com.example.currencyconverter.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "converter_local")
data class ConverterLocal(
    @field:ColumnInfo(name = "source") var source: String="",
    @field:ColumnInfo(name = "time_stamp") var timeStamp: Long = -1L,
    @field:ColumnInfo(name = "quote_json") var quoteJson : String,

    @field:ColumnInfo(name = "id",defaultValue = "0")
    @PrimaryKey(autoGenerate = true)
    var id :Int= 0
)