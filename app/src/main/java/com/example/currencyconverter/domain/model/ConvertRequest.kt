package com.example.currencyconverter.domain.model

data class ConvertRequest(
    val source : String,
    val format : Long
)
