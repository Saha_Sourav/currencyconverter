package com.example.currencyconverter.domain.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Converter(
    val success : Boolean,
    val error : Error?,
    val timestamp : Long?,
    val source : String?,
    @Expose
    @SerializedName("quotes")
    val quotes : HashMap<String,Float>?
)
