package com.example.currencyconverter.util

enum class Status {
    SUCCESS,
    ERROR,
    EXCEPTION
}