package com.example.currencyconverter.util

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper


object Utils {

    fun unwrap(context: Context): Activity? {
        var activityContext: Context? = context
        while (activityContext !is Activity && activityContext is ContextWrapper) {
            activityContext = activityContext.baseContext
        }
        return activityContext as Activity?
    }


}