package com.example.currencyconverter.repositories

import com.example.currencyconverter.data.repositories.ConverterRepository
import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.Converter
import com.example.currencyconverter.domain.model.ConverterLocal
import com.example.currencyconverter.util.Resource
import com.example.currencyconverter.utils.Constants

class FakeConverterRepositoryImpl : ConverterRepository {
    private var responseType = "success"
    private var code = 400
    private var converterLocalData : ConverterLocal? = null
    override suspend fun makeConvertRequest(convertRequest: ConvertRequest): Resource<Converter> {
        if(converterLocalData != null){
            return Resource.success(Converter(true,null,1L,"USD",HashMap()))
        }else{
            when (responseType) {
                "success" -> {
                    return Resource.success(Converter(true,null,1L,"USD",HashMap()))
                }
                "failure" -> {
                    return when (code) {
                        400 -> {
                            Resource.error("Bad request", null)
                        }
                        500 -> {
                            Resource.error("Internal server error", null)
                        }
                        else -> {
                            Resource.error("Something went wrong", null)
                        }
                    }
                }
                else -> {
                    return if (responseType == Constants.responseTypeTimeOutException ||
                        responseType == Constants.responseTypeUnknownHostException ||
                        responseType == Constants.socketTimeoutException
                    ) {
                        Resource.exception("No network connection ,  Please Check your connection", null)
                    } else {
                        Resource.exception("Something went wrong", null)

                    }
                }
            }
        }
    }


    fun setResponseType(responseType : String){
        this.responseType = responseType
    }

    fun setResponseCode(code : Int){
        this.code = code
    }

    fun setNeedToCallApiWithNullLocalVal(converterLocal: ConverterLocal?){
        this.converterLocalData = converterLocal
    }
}