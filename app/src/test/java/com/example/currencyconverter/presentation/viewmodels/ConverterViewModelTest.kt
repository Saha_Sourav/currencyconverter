package com.example.currencyconverter.presentation.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.currencyconverter.domain.model.ConvertRequest
import com.example.currencyconverter.domain.model.ConverterLocal
import com.example.currencyconverter.framework.presentation.viewmodels.ConverterViewModel
import com.example.currencyconverter.getOrAwaitValue
import com.example.currencyconverter.interactors.ConvertUseCase
import com.example.currencyconverter.repositories.FakeConverterRepositoryImpl
import com.example.currencyconverter.utils.Constants
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class ConverterViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var converterViewModel: ConverterViewModel
    private lateinit var fakeConverterRepositoryImpl: FakeConverterRepositoryImpl


    @Before
    fun setup() {
        fakeConverterRepositoryImpl = FakeConverterRepositoryImpl()
        converterViewModel = ConverterViewModel(ConvertUseCase(fakeConverterRepositoryImpl))
    }

    @Test
    fun `get conversion rate failure with 400 error`() = runBlockingTest {

        fakeConverterRepositoryImpl.setResponseType("failure")
        fakeConverterRepositoryImpl.setResponseCode(400)
        fakeConverterRepositoryImpl.setNeedToCallApiWithNullLocalVal(null)
        converterViewModel.makeConvertRequest(ConvertRequest("USD",1))
        val response = converterViewModel.converterLiveData.getOrAwaitValue()

        Truth.assertThat(response.message).isEqualTo("Bad request")
    }

    @Test
    fun `get conversion rate failure with 500 error`() = runBlockingTest {

        fakeConverterRepositoryImpl.setResponseType("failure")
        fakeConverterRepositoryImpl.setResponseCode(500)
        fakeConverterRepositoryImpl.setNeedToCallApiWithNullLocalVal(null)

        val response = fakeConverterRepositoryImpl.makeConvertRequest(ConvertRequest("USD",1))

        Truth.assertThat(response.message).isEqualTo("Internal server error")
    }

    @Test
    fun `get conversion rate failure with unknown error`() = runBlockingTest {

        fakeConverterRepositoryImpl.setResponseType("failure")
        fakeConverterRepositoryImpl.setResponseCode(0)
        fakeConverterRepositoryImpl.setNeedToCallApiWithNullLocalVal(null)

        converterViewModel.makeConvertRequest(ConvertRequest("USD",1))
        val response = converterViewModel.converterLiveData.getOrAwaitValue()

        Truth.assertThat(response.message).isEqualTo("Something went wrong")
    }


    @Test
    fun `get conversion rate failure with network exception`() = runBlockingTest {

        fakeConverterRepositoryImpl.setResponseType(Constants.responseTypeTimeOutException)
        fakeConverterRepositoryImpl.setResponseCode(0)
        fakeConverterRepositoryImpl.setNeedToCallApiWithNullLocalVal(null)

        converterViewModel.makeConvertRequest(ConvertRequest("USD",1))
        val response = converterViewModel.converterLiveData.getOrAwaitValue()
        Truth.assertThat(response.message).isEqualTo("No network connection ,  Please Check your connection")
    }


    @Test
    fun `get conversion rate success with local data`() = runBlockingTest {

        fakeConverterRepositoryImpl.setNeedToCallApiWithNullLocalVal(ConverterLocal("",1L,""))

        converterViewModel.makeConvertRequest(ConvertRequest("USD",1))
        val response = converterViewModel.converterLiveData.getOrAwaitValue()

        Truth.assertThat(response.data).isNotNull()
    }

    @Test
    fun `validate input returns false with empty input`(){
        val verdict = converterViewModel.validateInput("")
        Truth.assertThat(verdict).isFalse()
    }

    @Test
    fun `validate input returns false with null input`(){
        val verdict = converterViewModel.validateInput(null)
        Truth.assertThat(verdict).isFalse()
    }

    @Test
    fun `validate input returns true with decimal input`(){
        val verdict = converterViewModel.validateInput("1.2")
        Truth.assertThat(verdict).isTrue()
    }

    @Test
    fun `validate input returns true with illegal input`(){
        val verdict = converterViewModel.validateInput("1.")
        Truth.assertThat(verdict).isFalse()
    }

}