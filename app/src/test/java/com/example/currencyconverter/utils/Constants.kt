package com.example.currencyconverter.utils

object Constants {

    const val responseTypeUnknownHostException = "Unknown host exception"
    const val responseTypeTimeOutException = "Time out exception"
    const val socketTimeoutException = "Socket timeout Exception"
}