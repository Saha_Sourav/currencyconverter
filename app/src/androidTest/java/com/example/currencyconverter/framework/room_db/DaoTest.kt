package com.example.currencyconverter.framework.room_db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.currencyconverter.domain.model.ConverterLocal
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class DaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: Database
    private lateinit var dao: Dao

    @Before
    fun setup(){
        database = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(),Database::class.java)
            .allowMainThreadQueries()
            .build()
        dao = database.dao()

    }
    @After
    fun tearDown(){
        database.close()
    }

    @Test
    fun insertRemoteConversionRateDataToDbTest() = runBlockingTest {
        val localConverter = ConverterLocal("USD",1L,"")
        val inserted = dao.insertRemoteConversionRateDataToDb(localConverter)
        Truth.assertThat(inserted).isGreaterThan(0)
    }

    @Test
    fun getConversionRateBySourceTest() = runBlockingTest {
        val localConverter = ConverterLocal("USD",1L,"")
        dao.insertRemoteConversionRateDataToDb(localConverter)
        dao.insertRemoteConversionRateDataToDb(localConverter)

        val conversionList = dao.getConversionRateBySource("USD")

        Truth.assertThat(conversionList).hasSize(2)

    }

    @Test
    fun updateConversionLocalDataTest() = runBlockingTest {
        val localConverter = ConverterLocal("USD",1L,"")
        dao.insertRemoteConversionRateDataToDb(localConverter)
        dao.updateConverterLocalData(-2L,"","USD")
        val listConversions = dao.getConversionRateBySource("USD")

        Truth.assertThat(listConversions?.get(0)?.timeStamp).isEqualTo(-2L)
    }
}